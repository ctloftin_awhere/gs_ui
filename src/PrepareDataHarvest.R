library(Hmisc)

PrepareDataHarvest <- function(ddir, inFile) {
  # Read in data, rename dates and calculate the date for the week
  # Farm, Ranch field,Acres,Crop,Variety,Planting_Date,Estimated_Harvest,
  #    Estimated Harvest_GDD,Days_Early,Days_Late
  
  crpIn <- data.table(read.csv(file = paste(ddir, inFile,'.csv', sep=""), sep=",", 
                               strip.white = TRUE,stringsAsFactors = FALSE, header=TRUE, 
                               na.strings = list("NULL", "NA")))
  crpIn <- crpIn[, c("Date_of_Prediction", "Region", "Planting", "Farm", "fieldNameOrig", "Heads", "Crop", "Variety", "Planting_Date", 
                     "Standard_Harvest_Date", "Estimated_Harvest_GDD", "Days_Early", "Days_Late",  "Actual_Harvest_Date"), with = FALSE]
  setnames(crpIn, "fieldNameOrig", "Field")
  crpIn$Crop <- "Iceberg"
  crpIn$Planting <- toupper(crpIn$Planting)
  crpIn$Farm[which(crpIn$Farm == "pioneer & duchy farm")] <- "pioneer & duchy"
  crpIn$Region <- capitalize(crpIn$Region)
  
  
  crpIn$Actual_Harvest_Date <- format(as.Date(crpIn$Actual_Harvest_Date), "%m/%d/%Y")
#   crpIn$Standard_Harvest_Date <- as.Date(crpIn$Standard_Harvest_Date, "%m/%d/%Y")
#   crpIn$Estimated_Harvest_GDD <- as.Date(crpIn$Estimated_Harvest_GDD, "%m/%d/%Y")
#   crpIn$Date_of_Prediction <- as.Date(crpIn$Date_of_Prediction, "%m/%d/%Y")
#   crpIn$Planting_Date <- as.Date(crpIn$Planting_Date, "%m/%d/%Y")
  
  #     w <- which(is.na(crpIn$Actual_Harvest_Date))
  #     
  setnames(crpIn, "Standard_Harvest_Date", "Estimated_Harvest")
  

  
  crpIn$Region <- gsub("(^|[[:space:]])([[:alpha:]])", "\\1\\U\\2", tolower(crpIn$Region), perl=TRUE)
  crpIn$Farm <- gsub("(^|[[:space:]])([[:alpha:]])", "\\1\\U\\2", tolower(crpIn$Farm), perl=TRUE)
  # Convert character dates to dates
  
  crpIn$Region <- replace(crpIn$Region, crpIn$Region == "East coast", "East Coast")
  crpIn$Crop <- replace(crpIn$Crop, crpIn$Crop == "Romaine Hearts", "RomaineHearts")
  
  
  crpIn <- crpIn[ ,`:=`( est = as.Date(Estimated_Harvest), estGDD = as.Date(Estimated_Harvest_GDD))]
  crpIn <- crpIn[ , c("Estimated_Harvest", "Estimated_Harvest_GDD"):= NULL]
  # They do not harvest on Sunday, so roll Sunday harvest over to Monday
  # Sunday is day one in wday
  crpIn <- crpIn[ , ':='(estWkDay = wday(est), estWkDayGDD = wday(estGDD))]
  #     crpIn <- crpIn[estWkDay == 1, est:= as.Date(est + days(1))]
  # Move all Sunday Harvests to Monday
  #     crpIn <- crpIn[estWkDayGDD == 1, estGDD:= as.Date(estGDD + days(1))]
  crpIn <- crpIn[ , c("estWkDayGDD", "estWkDay") := NULL]
  crpIn$Planting_Date <- as.Date(crpIn$Planting_Date)
  crpIn <- crpIn[ ,`:=`(minDate = pmin(est, estGDD), maxDate = pmax(est,estGDD))]
  
  setnames(crpIn, "Heads", "Acres")
  crpIn$Acres <- as.numeric(crpIn$Acres)
  crpIn <- crpIn[!is.na(Acres),]
  return(crpIn)
}

ReadBenchmarks <- function(ddir, inBench) {
  #  Temporary algorithm until we have correct file for benchmarks  
  
  crpBench <- data.table(read.csv(file = paste(ddir, inBench,'.csv', sep=""), sep=",", 
                                  strip.white = TRUE,stringsAsFactors = FALSE, header=TRUE, 
                                  na.strings = list("NULL", "NA")))
  crpBench <- crpBench[ , strTuesday:= paste('2015', as.character(crpBench$Week),'3')]
  crpBench <- crpBench[ , theDay :=  as.Date(strTuesday,"%Y %U %u")]
  crpBench <- crpBench[ , strTuesday := NULL]
  crpBench <- crpBench[ , Harvest := "Benchmark"]  
  
  crpBenchmarks <- crpBench %>% gather(Crop, Acres, -theDay, -Harvest, - Week)
  ### Gather turns Crop into a factor, undo this
  crpBenchmarks$Crop <- as.character(crpBenchmarks$Crop) 
  setcolorder(crpBenchmarks,c('Week', 'Crop', 'theDay', 'Harvest', 'Acres'))
  setorder(crpBenchmarks, Crop, Week, theDay)
  return(crpBenchmarks)
}   
