#Make the Tuesday Report
MakeTuesdayReport <- function(crpAll, crpBenchmarks) {
  
  # Make Day Report   
  crpAll <- crpAll[, c("Crop", "Acres", "est", "estGDD", "Actual_Harvest_Date"), with=FALSE]
  crpAll$estWk <- getWeekOfYear(crpAll$est, "2014-12-29")
  crpAll$estWkGDD <- getWeekOfYear(crpAll$estGDD, "2014-12-29")
  
  startWeek <- getWeekOfYear(Sys.Date(), "2014-12-29")
  endWeek <- startWeek + 3
  thisMonday <- getDateOfDayInWeek(2, startWeek)$theDays
  nextTuesday <- thisMonday + days(8)
  dailyDates <- seq(thisMonday, nextTuesday, by="days")
  crops <- c("Broccoli", "Cauliflower", "Celery", "Iceberg", "Romaine", "RomaineHearts")
  
  est <- crpAll[estWk >= startWeek & estWk <= endWeek]
  est <- est[, c("Crop", "Acres", "est", "estWk"), with=FALSE]
  gdd <- crpAll[estWkGDD >= startWeek & estWkGDD <= endWeek & is.na(Actual_Harvest_Date)]
  gdd <- gdd[, c("Crop", "Acres", "estGDD", "estWkGDD"), with=FALSE]
  setnames(est, "est", "theDay")
  setnames(gdd, "estGDD", "theDay")

  
  dailyEst <- est[theDay >= thisMonday & theDay <= nextTuesday]
  dailyGdd <- gdd[theDay >= thisMonday & theDay <= nextTuesday]
  
  dailyEst <- dailyEst[, .(Acres=sum(Acres)), by=.(Crop, theDay)]
  dailyGdd <- dailyGdd[, .(Acres=sum(Acres)), by=.(Crop, theDay)]
  
  fillerEst <- data.table(Crop=rep(crops, each=9), theDay = dailyDates, Acres = 0)
  fillerGdd <- data.table(Crop=rep(crops, each=9), theDay = dailyDates, Acres = 0)
  
  for(i in 1:54) {
    tempEstAcres <- length(dailyEst[theDay==fillerEst$theDay[i] & Crop==fillerEst$Crop[i]]$Acres)
    tempGddAcres <- length(dailyGdd[theDay==fillerGdd$theDay[i] & Crop==fillerGdd$Crop[i]]$Acres)
    if(tempEstAcres!=0) {
      fillerEst$Acres[i] <- dailyEst[theDay==fillerEst$theDay[i] & Crop==fillerEst$Crop[i]]$Acres
    }
    if(tempGddAcres!=0) {
      fillerGdd$Acres[i] <- dailyGdd[theDay==fillerGdd$theDay[i] & Crop==fillerGdd$Crop[i]]$Acres
    }
  }
  
  fillerEst$theWeekday <- wday(fillerEst$theDay, label=TRUE, abbr=TRUE)
  fillerGdd$theWeekday <- wday(fillerGdd$theDay, label=TRUE, abbr=TRUE)
  
  dailyEst <- as.data.frame(fillerEst)
  dailyGdd <- as.data.frame(fillerGdd)
  
  dailyEst <- reshape(dailyEst, idvar="Crop", timevar="theDay", direction="wide")
  dailyGdd <- reshape(dailyGdd, idvar="Crop", timevar="theDay", direction="wide")
  
  dailyEst <- dailyEst[, -grep("theWeekday", colnames(dailyEst))]
  dailyGdd <- dailyGdd[, -grep("theWeekday", colnames(dailyGdd))]
  
  colnames(dailyEst) <- gsub("Acres.", "", colnames(dailyEst))
  colnames(dailyGdd) <- gsub("Acres.", "", colnames(dailyGdd))
  
  dailyEst$Harvest <- "Planned"
  dailyGdd$Harvest <- "Projected (GDD)"
  
  demand <- dailyEst
  demand[,c(2:10)] <- "-"
  demand$Harvest = "Demand"
  
  toReturn <- as.data.frame(matrix(ncol=11, nrow=19))
  setnames(toReturn, colnames(demand))
  toReturn[1,c(2:10)] <- as.character(format(dailyDates, "%b %d"))
  
  for(i in 1:6) {
    toReturn[(i*3-1),] <- demand[i,]
    toReturn[(i*3),] <- dailyEst[i,]
    toReturn[(i*3+1),] <- dailyGdd[i,]
  }
  toReturn[is.na(toReturn)] <- ""
  temp <- toReturn[, c(2:10)]
  toReturn <- toReturn[,-c(2:10)]
  toReturn <- cbind(toReturn, temp)
  setnames(toReturn, colnames(toReturn[c(3:11)]), as.character(wday(dailyDates, label=TRUE, abbr=TRUE)))
  
  est <- est[, .(Acres=sum(Acres)), by=.(Crop,estWk)]
  gdd <- gdd[, .(Acres=sum(Acres)), by=.(Crop,estWkGDD)]
  
  fillerEst <- data.table(Crop=rep(crops, each=4), theWeek = c(startWeek:endWeek), Acres = 0)
  fillerGdd <- data.table(Crop=rep(crops, each=4), theWeek = c(startWeek:endWeek), Acres = 0)
  
  for(i in 1:24) {
    tempEstAcres <- length(est[estWk==fillerEst$theWeek[i] & Crop==fillerEst$Crop[i]]$Acres)
    tempGddAcres <- length(gdd[estWkGDD==fillerGdd$theWeek[i] & Crop==fillerGdd$Crop[i]]$Acres)
    if(tempEstAcres!=0) {
      fillerEst$Acres[i] <- est[estWk==fillerEst$theWeek[i] & Crop==fillerEst$Crop[i]]$Acres
    }
    if(tempGddAcres!=0) {
      fillerGdd$Acres[i] <- gdd[estWkGDD==fillerGdd$theWeek[i] & Crop==fillerGdd$Crop[i]]$Acres
    }
  }
  
  fillerEst <- reshape(fillerEst, idvar="Crop", timevar="theWeek", direction="wide")
  fillerGdd <- reshape(fillerGdd, idvar="Crop", timevar="theWeek", direction="wide")
  colnames(fillerEst) <- gsub("Acres.", "", colnames(fillerEst))
  colnames(fillerGdd) <- gsub("Acres.", "", colnames(fillerGdd))
  
  benchmarks <- crpBenchmarks[Week >= startWeek & Week <= endWeek & Crop %in% crops]
  benchmarks <- as.data.frame(reshape(benchmarks, idvar="Crop", timevar="Week", direction="wide"))
  benchmarks <- benchmarks[, -grep("theDay.", colnames(benchmarks))]
  benchmarks <- benchmarks[, -grep("Harvest.", colnames(benchmarks))]
  colnames(benchmarks) <- gsub("Acres.", "", colnames(benchmarks))
  
  toReturnWeekly <- as.data.frame(matrix(ncol=5, nrow=19))
  setnames(toReturnWeekly, c("Crop",as.character(c(startWeek:endWeek))))
  for(i in 1:6) {
    toReturnWeekly[(i*3-1),] <- benchmarks[i,]
    toReturnWeekly[(i*3),] <- fillerEst[i,]
    toReturnWeekly[(i*3+1),] <- fillerGdd[i,]
  }
  toReturnWeekly[1,c(2:5)] <- as.character(format(getDateOfDayInWeek(2, colnames(toReturnWeekly))$theDays, "%b %d"))
  toReturnWeekly$Crop <- NULL
  
  toReturn <- cbind(toReturn,toReturnWeekly)
  
  toReturnWeekly$Crop <- ""
  toReturnWeekly$Harvest <- ""
  toReturnWeekly$Crop[c(2:19)] <- toReturn$Crop[c(2:19)]
  toReturnWeekly$Harvest[c(2:19)] <- toReturn$Harvest[c(2:19)]
  toReturnWeekly <- as.data.table(toReturnWeekly)
  temp <- toReturnWeekly[, c("Crop", "Harvest"), with=FALSE]
  temp2 <- toReturnWeekly[, colnames(toReturnWeekly)[c(1:4)], with=FALSE]
  toReturnWeekly <- cbind(temp, temp2)
  
  tuesReports <- list(toReturn, toReturnWeekly)
  names(tuesReports) <- c('theTuesTable', 'theTuesWeekly')
  
  return(tuesReports)
}