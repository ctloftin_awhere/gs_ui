# Make Filler
# There may be some days without harvest; these need to be included in the graphs.
MakeFillerCropVariety <- function(thisCrop, begEndList, numWks) {
  # Make day filler
  
  # What crops are in this data?
  cropVariety <- unique(thisCrop[ ,    .(Crop, Variety)])
  dayList <-  begEndList[["dayList"]]
  
  # Need to format as a date for combining with other data
  dayList <- dayList[,theDay := as.Date(theDay)]
  
  # Make enough crop variety information for all the dates
  dayFill <- data.frame(cbind(cropVariety, est = dayList$theDay[1], estGDD = dayList$theDay[1],
                              estWk = dayList$weekNum[1], estWkGDD = dayList$weekNum[1]))
  
  for(i in  2:nrow(dayList)){
    y <- data.frame(cbind(cropVariety,est = dayList$theDay[i],estGDD = dayList$theDay[i],
                          estWk = dayList$weekNum[i], estWkGDD = dayList$weekNum[i]))
    dayFill <- rbind(dayFill,y)
  }
  dayList$est <- dayList$theDay
  dayList$estGDD <- dayList$theDay
  
  # Fill in week number on real data
  if(numWks == 0) {
    thisCrop$theHarvestDay <- as.Date(thisCrop$theHarvestDay)
    thisCrop$thePlannedDay <- as.Date(thisCrop$thePlannedDay)
    setkey(thisCrop, theHarvestDay)
    setkey(dayList, theDay)
    
    thisEst <- thisCrop[dayList, nomatch = 0]
    
    setkey(thisEst, thePlannedDay)
    thisEstGDD <- thisEst[dayList, nomatch = 0]
    
    setnames(thisEstGDD, c("weekNum", "i.weekNum"), c("actualWeek", "plannedWeek"))
    thisData <- thisEstGDD
    thisData <- thisData[, c("Field", "Acres", "Crop", "Variety", "Planting_Date", "Days_Early", "Days_Late", "Notes", "Actual_Harvest_Date",
                             "est", "estGDD", "theHarvestDay", "thePlannedDay", "actualWeek", "plannedWeek"), with=FALSE]
  } else {
    setkey(thisCrop,est)
    setkey(dayList, theDay)
    
    thisEst <- dayList[thisCrop]
    thisEst[, c("theDay", "estGDD") := NULL]
    setnames(thisEst, c("weekNum", "i.estGDD"), c("estWk", "estGDD"))
    
    setkey(thisEst,estGDD)
    
#     setkey(thisEst,estGDD)
    thisEstGDD <- dayList[thisEst]
    thisEstGDD[, c("theDay", "est") := NULL]
    setnames(thisEstGDD, c("weekNum", "i.est"), c("estWkGDD", "est"))
    
    # Combine day filler with data    
#     thisData <- rbindlist(list(thisEstGDD, dayFill), fill= TRUE, use.names = TRUE)
    thisData <- thisEstGDD[is.na(Acres), Acres := 0]
  }
  


  #  write.csv(thisData, file="data//thisData.csv", row.names=FALSE)
  
  return(thisData)
}