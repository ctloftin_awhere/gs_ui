getDateFromYDay <- function(days, year) {
  
  days <- as.data.table(as.integer(days))
  setnames(days, "days")
  days$rowNum = c(1:nrow(days))
  days <- data.table(days, key = "days")

  
  allDays <- as.data.frame(matrix(ncol = 0, nrow = as.numeric(difftime(paste0(year,"-12-31"), paste0(year,"-01-01"), units = 'days')) + 1))
  allDays$yearDays <- seq(as.Date(paste0(year,"-01-01")),as.Date(paste0(year,"-12-31")), by="days")
  allDays$yday <- yday(allDays$yearDays)
  allDays <- data.table(allDays, key = "yday")
  
  toReturn <- allDays[days]
  toReturn <- toReturn[order(toReturn$rowNum),]
  return(toReturn$yearDays)
}