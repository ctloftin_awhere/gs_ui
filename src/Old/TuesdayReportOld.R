
require(data.table)
require(plyr)
require(lubridate)
require(ggplot2)
require(scales)
require(ISOweek)
require(tidyr)
require(zoo)
options(stringsAsFactors = FALSE)


source("src//SummarizeByDayWeek.R")
#source("src//SummarizeByDay.R")
source("src//FieldPlots.R")
source("src//SummaryPlots.R")
source("src//PrepareDataTables.R")
source('src//PrepareDataMarch.R')
source("src//CalcDaysWks.R")


# Read CSV file
    ddir <- 'data//'
    # Assuming a csv file
    inFile <- "forColin_2012_03022015v2"
    
    #  We need a beginning date - this should be tied to work schedule (Sunday)
    begDate <- "2012-06-06"
    numWks <- 6
    theCrop <- "Lettuce"
    theVariety <- 'All'
 
    # Read in Data and calculate some dates & weeks
    crpAll <- PrepareDataMarch(ddir, inFile) 
    
    # Calculate begin and end dates and list of day and week placeholders 
    begEnd <- CalcDaysWks(begDate, numWks)
    begDay <- begEnd[['begDay']]
    endDay <- begEnd[['endDay']]
    
    # Extract appropriate data for time frame
    thisTime <- crpAll[minDate > begDay - days(1)  & maxDate < endDay + days(1)]
    
    # Filter the data for crop and variety
    thisCrop <- thisTime[ , c("minDate", "maxDate") := NULL]
    if(!theCrop == "All") {thisCrop <- thisCrop[Crop == theCrop]}
    if(!theVariety == 'All') { thisCrop <- thisCrop[variety == theVariety]}
 
    thisData <- MakeFiller(thisCrop, begEnd)
    byDaysCrop <- SummarizeByDay(thisData) 
    byWksCrop <- SummarizeByWeek(thisData)
    tuesdayReportf <- MakeTuesdayReport(byDaysCrop, byWksCrop) 

    byDaysTableOneCrop <- DataTableDayOneCrop(thisCrop)
    byDaysTableManyCrops <- DataTableDayManyCrops(thisCrop)

