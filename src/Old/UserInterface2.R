require(data.table)
require(plyr)
require(lubridate)
require(ggplot2)
require(scales)
require(ISOweek)
require(tidyr)
require(zoo)
options(stringsAsFactors = FALSE)


source("src//SummarizeByWeek.R")
source("src//SummarizeByDay.R")
source("src//FieldPlots.R")
source("src//SummaryPlots.R")
source("src//PrepareDataTables.R")
source('src//PrepareData.R')
source("src//CalcBegEnd.R")

# Read CSV file
    ddir <- 'data//'
    # Assuming a csv file
    inFile <- "forColin_2012_03022015v2"
  
#  We need a beginning date - this should be tied to work schedule (Sunday)
    begDate <- "2012-06-06"
    numWks <- 4
    theCrop <- "Broccoli"
    theVariety <- 'All'
    
    begEnd <- CalcBegEnd(begDate, numWks)
    # Read in Data and calculate some dates & weeks
    crpAll <- PrepareData(ddir, inFile) 

# Filter the data for crop and time frame
    thisData <- crpAll
    if(!theCrop == "All") {thisData <- thisData[Crop == theCrop]}
    thisData <-  thisData[mindate > begEnd$begDay & maxdate < begEnd$endDay]
    if(!theVariety == 'All') { thisData <- thisData[variety == theVariety]}

# Summarize the data by week
   weekSumf <- SummarizeByWeek(thisData)

# Summarize the data by week
   daySumf <- SummarizeByDay(thisData)

# Summary Plots
    # By day with every or every other day labelled
    g <- SummarybyDayOneDay(daySumf, begEnd)
    g <- SummarybyDayOneDay(daySumf, begEnd)
    print(g)
    g <- SummarybyDayTwoDays(daySumf, begEnd)
    print(g)
    # By week summary
    g <- SummaryByWeek(weekSumf)
    print(g)

#  by Field Plots
    # if you want every day labeled in the plot
    g <- FieldPlotOneDay(thisData, begEnd) 
    print(g)
    # if you only want every other day labeled in the plot
    g <- FieldPlotTwoDays(thisData, begEnd) 
    print(g)
 
# For data tables to display on web interface.
    displayData <- DataTableDay(thisData)
    setcolorder(displayData, c('Field', 'Variety', 'Acres','Planting Date',
                'Estimated','Estimated (GDD)', 'Days Early','Days Late'))
    
    # This table includes crop
    displayDataCrop <- DataTableDayCrop(thisData)
    setcolorder(displayDataCrop, c('Field', 'Crop','Variety', 'Acres','Planting Date',
                'Estimated','Estimated (GDD)', 'Days Early','Days Late'))
  
