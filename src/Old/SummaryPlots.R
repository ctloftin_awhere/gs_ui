SummarybyDayOneDay <- function(daysSumf, begEnd) {
    
    g <-  ggplot(data=daysSumf) + 
                geom_bar(aes(x = as.Date(theDay), y = Acres, fill = factor(Harvest)), 
                         stat = "identity", position = "dodge") +
                xlab("") +  
                scale_x_date(limits =c(as.Date(begEnd$begDay),as.Date(begEnd$endDay)),
                        labels = date_format(format = "%b %d"), 
                        breaks = date_breaks("1 day")) +                  
                scale_fill_manual(values=c("skyblue", "green"), 
                    name="", labels = c("Estimated","Estimated (GDD)")) +
                theme_bw(base_size=12) +
                theme(legend.position = "top") 
return(g)
}

SummarybyDayTwoDays <- function(daysSumf, begEnd) {
    
    g <-  ggplot(data=daysSumf) + 
                geom_bar(aes(x = as.Date(theDay), y = acres, fill = factor(acType)), 
                         stat = "identity", position = "dodge") +
                xlab("") +  
                scale_x_date(limits =c(as.Date(begEnd$begDay),as.Date(begEnd$endDay)),
                        labels = date_format(format = "%b %d"), 
                        breaks = date_breaks("2 days")) +                  
                scale_fill_manual(values=c("skyblue", "green"), 
                    name="", labels = c("Estimated","Estimated (GDD)")) +
                theme_bw(base_size=12) +
                theme(legend.position = "top") 

return(g)
}

SummaryByWeek <- function(weekSumf, begEnd) {
    g <-  ggplot(data=weekSumf) + 
                geom_bar(aes(x = as.Date(weekDate), y = acres, fill = factor(acType)), 
                         stat = "identity", position = "dodge") +
                xlab("") +  
                scale_x_date(labels = date_format(format = "%b %d"), 
                breaks = date_breaks("1 week")) +
                scale_fill_manual(values=c("skyblue", "green"), 
                    name="",labels=c("Estimated", "Estimated (GDD)")) +
                theme_bw(base_size=12)+
                theme(legend.position = "top") 
        
                
return(g)
}
